# Terraform setup
terraform {
  required_version = ">= 1.5.0"
  backend "s3" {
    region = "us-west-2"
  }
}

provider "aws" {
  region = "us-west-2"
}

# Resources
resource "random_string" "asg" {
  length  = 6
  special = false
  upper   = false
  numeric = true
}

resource "aws_iam_instance_profile" "asg" {
  name = "asg-${random_string.asg.id}"
  role = aws_iam_role.asg.name
}

resource "aws_iam_role" "asg" {
  name               = "asg-${random_string.asg.id}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Principal": {
            "Service": "ec2.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
    }
  ]
}
EOF
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
  ]
  inline_policy {
    name   = "asg-${random_string.asg.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:AttachVolume",
                "ec2:DettachVolume",
                "ec2:DescribeVolumes",
                "ec2:AssociateAddress",
                "ec2:DisassociateAddress",
                "ec2:DescribeAddresses",
                "ec2:DescribeInstances"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
    ]
}
EOF
  }
}

data "aws_eip" "asg" {
  public_ip = var.eip_address
}

resource "aws_launch_template" "asg" {
  name                   = "asg-${random_string.asg.id}"
  image_id               = var.ami_id
  vpc_security_group_ids = [aws_security_group.asg.id]
  user_data = base64encode(templatefile("userdata.sh", {
    ebs_volume_id     = var.ebs_volume_id,
    eip_allocation_id = data.aws_eip.asg.id,
  }))

  iam_instance_profile {
    name = aws_iam_instance_profile.asg.name
  }
  key_name = var.key_name

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg" {
  name                = "asg-${random_string.asg.id}"
  desired_capacity    = 1
  min_size            = 0
  max_size            = 1
  vpc_zone_identifier = [var.subnet_id]

  mixed_instances_policy {
    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.asg.id
        version            = "$Latest"
      }
      dynamic "override" {
        for_each = var.instance_types
        content {
          instance_type = override.value
        }
      }
    }
    instances_distribution {
      on_demand_base_capacity                  = 0
      on_demand_percentage_above_base_capacity = 0
      spot_allocation_strategy                 = "lowest-price"
    }
  }

  tag {
    key                 = "Name"
    value               = "asg-${random_string.asg.id}"
    propagate_at_launch = true
  }
}

resource "aws_scheduler_schedule" "asg_scheduler" {
  name                         = "asg-${random_string.asg.id}"
  schedule_expression          = var.stop_schedule
  schedule_expression_timezone = "Pacific/Auckland"
  flexible_time_window {
    mode = "OFF"
  }
  target {
    role_arn = aws_iam_role.asg_scheduler.arn
    arn      = "arn:aws:scheduler:::aws-sdk:autoscaling:setDesiredCapacity"
    input = jsonencode({
      DesiredCapacity      = 0
      AutoScalingGroupName = aws_autoscaling_group.asg.name
    })
  }
}

resource "aws_iam_role" "asg_scheduler" {
  name               = "asg-${random_string.asg.id}-scheduler"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Principal": {
            "Service": "scheduler.amazonaws.com"
        },
        "Effect": "Allow"
    }
  ]
}
EOF
  inline_policy {
    name   = "autoscaling"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "autoscaling:SetDesiredCapacity"
            ],
            "Effect": "Allow",
            "Resource": "${aws_autoscaling_group.asg.arn}"
        }
    ]
}
EOF
  }
}

resource "aws_security_group" "asg" {
  name        = "asg-${random_string.asg.id}"
  description = "Allow 22 and 7890"
  vpc_id      = var.vpc_id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.source_ips
  }
  ingress {
    from_port   = 7890
    to_port     = 7890
    protocol    = "tcp"
    cidr_blocks = var.source_ips
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Variables
variable "source_ips" {
  description = "The IP address to allow SSH access from"
  default     = ["60.234.24.234/32"]
}

variable "instance_types" {
  description = "The type of instance to launch"
  default     = ["g5.2xlarge", "g5.4xlarge"]
}

variable "key_name" {
  description = "The key pair to use for the instance"
  default     = "aws-sandpit"
}

variable "region" {
  description = "The region to launch the instance in"
  default     = "us-west-2"
}

variable "ebs_volume_id" {
  description = "The EBS volume ID to attach to the instance"
}

variable "ami_id" {
  description = "The AMI ID to use for the instance"
}

variable "vpc_id" {
  description = "The VPC ID to use for the instance"
}

variable "subnet_id" {
  description = "The subnet ID to use for the instance"
}

variable "eip_address" {
  description = "The EIP address to use for the instance"
}

variable "stop_schedule" {
  description = "Cron schedule for stopping the instance"
}

# Outputs
output "asg_public_ip" {
  value = data.aws_eip.asg.public_ip
}
