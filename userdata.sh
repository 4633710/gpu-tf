#!/bin/bash -x

# Retrieve instance identity document
ID_DOC=$(mktemp -t id-doc.XXXXXXXXXX.json)
TOKEN=$(curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600")
curl -H "X-aws-ec2-metadata-token: $TOKEN" -s http://169.254.169.254/latest/dynamic/instance-identity/document > $${ID_DOC}

export AWS_DEFAULT_REGION=$(jq -r .region $${ID_DOC})

# Attach EBS
INSTANCE_ID=$(jq -r .instanceId $${ID_DOC})
aws ec2 attach-volume --volume-id ${ebs_volume_id} --instance-id $${INSTANCE_ID} --device /dev/sdf

# Wait for EBS to attach
for i in {1..60}; do
    sleep 2
    STATE=$(aws ec2 describe-volumes --volume-ids ${ebs_volume_id} | jq -r '.Volumes[0].Attachments[0].State')
    if [ "$${STATE}" == "attached" ]; then
        break
    fi
done

if [ "$${STATE}" != "attached" ]; then
    echo "EBS volume did not attach in time"
fi

# Add /dev/nvme1n1p1 to /etc/fstab
echo "LABEL=home /home ext4 defaults,noatime 0 0" >> /etc/fstab
mount -a

# Associate Elastic IP
aws ec2 associate-address --instance-id $${INSTANCE_ID} --allocation-id ${eip_allocation_id} --allow-reassociation

chmod +x /etc/rc.d/rc.local
CMD="sudo -u ec2-user screen -d -m /home/ec2-user/sd/run-sd.sh"
echo "$${CMD}" >> /etc/rc.d/rc.local
$${CMD}

echo "Done"
